﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;

namespace ShultTable
{
    public partial class Form1 : Form
    {
        public int[] Area = new int[32];
        public int Status = 0;
        public int CurrentInt = 1;
        public int SetCountTimeBlink = 3;
        public Button CurrentButton;
        public int Secunda = 0;
        public int Minute = 0;
        public string ButtonLastText = "";
        public string BestRecord = "";

        // Событие происходящие при нажатии кнопки
        public int ClickButton(Button btn)
        {
            if (CurrentInt == 25)
            {
                Finish();
                return 0;
            }

            if (timer2.Enabled)
            {
                timer2.Enabled = false;
                SetCountTimeBlink = 3;
                CurrentButton.Image = null;
                CurrentButton.Text = ButtonLastText;
            }
            CurrentButton = btn;
            string text = btn.Text;
            if (text != "")
            {
                int Cliked = Convert.ToInt16(text);
                if (Cliked == CurrentInt)
                {
                    PaintIconOnButton(1);
                    CurrentInt++;
                    if (CurrentInt!=25) return 1;
                }
                else
                {
                    PaintIconOnButton(0);
                    return 0;
                }
            }
            

            return 2;
        }

        public void InitNew()
        {
            pictureBox2.Visible = false;
            Secunda = 0;
            Minute = 0;
            CurrentInt = 1;
            label1.Text = "00:00";
            label3.Visible = false;
            label2.Visible = false;
        }

        public void Finish()
        {
            pictureBox2.Visible = true;
            timer1.Enabled = false;
            pictureBox2.Location = new Point(118, 25);
            Status = 0;
            try
            {
                BestRecord = TextDeCrypt(Properties.Settings.Default.BestRecord);

            } catch(Exception Ex){
                MessageBox.Show(Ex.Message);
            }
            string CurrentTime = label1.Text;

            int time1 = TimeToSec(BestRecord);
            int time2 = TimeToSec(CurrentTime);

            if (time1 > time2)
            {
                label3.Text = "Новый Рекод!";
                label2.Text = label1.Text;
                button26.Text = "Установим новый рекорд?";

                Properties.Settings.Default.BestRecord = TextCrypt(label1.Text);
                Properties.Settings.Default.Save();
            }
            else
            {
                label3.Text = "Лучший результат";
                label2.Text = BestRecord;
                button26.Text = "Побъем текущий рекорд?"; 
            }

            label3.Visible = true;
            label2.Visible = true;
        }

        public int TimeToSec(string Text)
        {
            if (Text != null && Text.Length == 5)
            {
                int minute = Convert.ToInt32(Text[0].ToString()) + Convert.ToInt32(Text[1].ToString());
                int secund = Convert.ToInt32(Text[3].ToString()) + Convert.ToInt32(Text[4].ToString());
                return minute * 60 + secund;
            }
            else
                return 9999999;
        }

        // Генерируем массив случайных чисел
        public int[] GenMassive()
        {
            int[] temp = new int[32];
            Random rd = new Random();
            int current = 0;
            int RandomNumber = 0;

            do{
                current++;
                RandomNumber = rd.Next(1,26);
                while (temp[RandomNumber] != 0)
                {
                    RandomNumber++;
                    if (RandomNumber > 25) RandomNumber = 1;
                }
                temp[RandomNumber] = current;
            } while(current!=25);

            return temp;
        }

        // Заполняем названия кнопок
        public void ButtonFill(int[] area)
        {
            if (area[1] != 0)
            {
                button1.Text = area[1].ToString();
                button2.Text = area[2].ToString();
                button3.Text = area[3].ToString();
                button4.Text = area[4].ToString();
                button5.Text = area[5].ToString();

                button6.Text = area[6].ToString();
                button7.Text = area[7].ToString();
                button8.Text = area[8].ToString();
                button9.Text = area[9].ToString();
                button10.Text = area[10].ToString();

                button11.Text = area[11].ToString();
                button12.Text = area[12].ToString();
                button13.Text = area[13].ToString();
                button14.Text = area[14].ToString();
                button15.Text = area[15].ToString();

                button16.Text = area[16].ToString();
                button17.Text = area[17].ToString();
                button18.Text = area[18].ToString();
                button19.Text = area[19].ToString();
                button20.Text = area[20].ToString();

                button21.Text = area[21].ToString();
                button22.Text = area[22].ToString();
                button23.Text = area[23].ToString();
                button24.Text = area[24].ToString();
                button25.Text = area[25].ToString();
            }
            else
            {
                button1.Text = "";
                button2.Text = "";
                button3.Text = "";
                button4.Text = "";
                button5.Text = "";
                button6.Text = "";
                button7.Text = "";
                button8.Text = "";
                button9.Text = "";
                button10.Text = "";
                button11.Text = "";
                button12.Text = "";
                button13.Text = "";
                button14.Text = "";
                button15.Text = "";
                button16.Text = "";
                button17.Text = "";
                button18.Text = "";
                button19.Text = "";
                button20.Text = "";
                button21.Text = "";
                button22.Text = "";
                button23.Text = "";
                button24.Text = "";
                button25.Text = "";
            }
        }

        // Рисуем на кнопке иконку
        public void PaintIconOnButton(int NumberIcon)
        {
            ButtonLastText = CurrentButton.Text;
            CurrentButton.Text = "";

            CurrentButton.Image = imageList1.Images[NumberIcon];
            timer2.Enabled = true;
        }

        // иницилизация формы
        public Form1()
        {
            InitializeComponent();

            var settings = Properties.Settings.Default;
            if (settings.NeedUpgrade)
            {
                settings.Upgrade();
                settings.NeedUpgrade = false;
                settings.Save();
            }
            
        }

        // кнопка старта 
        private void button26_Click(object sender, EventArgs e)
        {
            if (Status == 0)
            {
                InitNew();
                Area = GenMassive();
                ButtonFill(Area);
                label1.Visible = true;
                label4.Visible = true;
                timer1.Enabled = true;
                Status = 1;
                button26.Text = "Остановить";
            }
            else
            {
                if (Status == 1)
                {
                    label1.Visible = false;
                    ButtonFill(new int[32]);
                    timer1.Enabled = false;
                    button26.Text = "Старт";
                    Status = 0;
                }
            }

        }

        // Событие от кнопок на форме
        private void button_MainClick(object sender, EventArgs e)
        {
            if (Status==1) 
                ClickButton(((Button)sender));
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            
            string OutputS = "00";
            string OutputM = "00";

            Secunda++;
            if (Secunda == 60)
            {
                Minute++;
                Secunda = 0;
            }

            if (Secunda < 10)
                OutputS = "0" + Secunda.ToString();
            else
                OutputS = Secunda.ToString();

            if (Minute < 10)
                OutputM = "0" + Minute.ToString();
            else
                OutputM = Minute.ToString();

            label1.Text = OutputM + ":" + OutputS;
        }
        // Обработка иконок
        private void timer2_Tick(object sender, EventArgs e)
        {

            if (SetCountTimeBlink == 0)
            {
                SetCountTimeBlink = 3;
                CurrentButton.Image = null;
                CurrentButton.Text = ButtonLastText;
                timer2.Enabled = false;
            }
            else
            {
                SetCountTimeBlink--;
            }
            
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private string TextCrypt(string text)
        {
            byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(text);
            string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);
            return returnValue;
        }

        private string TextDeCrypt(string text)
        {
            byte[] encodedDataAsBytes = System.Convert.FromBase64String(text);
            string returnValue = System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);
            return returnValue;
        }

        private void label5_Click(object sender, EventArgs e)
        {
            string TextMessage = "Таблица Шульта - программа для тренировки внимания" + Environment.NewLine + Environment.NewLine +
                "После нажатия кнопки старт запускается таймер и" + Environment.NewLine +
                "вам будет необходимо найти и нажать кнопки " + Environment.NewLine +
                "в последовательности от 1 до 25, чем быстрее " + Environment.NewLine +
                "тем лучше :-)" + Environment.NewLine + Environment.NewLine +
                "Автор: Наумко Дмитрий (playlinsor@gmail.com)" + Environment.NewLine +
                "Специально для Алима Мамутова(Strodis)";
            
            MessageBox.Show(TextMessage,"Помощь");
        }

        private void label5_MouseMove(object sender, MouseEventArgs e)
        {
            label5.BackColor = Color.White;
            label5.ForeColor = Color.Black;
        }

        private void label5_MouseLeave(object sender, EventArgs e)
        {
            label5.BackColor = Color.Black;
            label5.ForeColor = Color.Lime;
        }
    }
}
